# Product Expiration Module
Adds a tool that allows customers to check the expiration date on their products.

Frontend is available at <base_url>/product-expiration/

Module is configured under Store > Configuration > Catalog > Expiration Tool
