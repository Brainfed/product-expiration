<?php

namespace Brainfed\ProductExpiration\Controller\Index;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\ScopeInterface;

class Index extends Action
{
    const XML_PATH_MODULE_ON = 'brainfed_expiration/general/enable';
    const XML_PATH_META_TITLE = 'brainfed_expiration/meta/meta_title';
    const XML_PATH_META_DESC = 'brainfed_expiration/meta/meta_description';
    const XML_PATH_META_KEYWORDS = 'brainfed_expiration/meta/meta_keywords';

    protected $session;
    protected $url;
    protected $message;
    protected $scopeConfig;
    protected $pageFactory;

    public function __construct(
        Context $context,
        Session $session,
        ScopeConfigInterface $scopeConfig,
        PageFactory $pageFactory
    )
    {
        $this->session = $session;
        $this->url = $context->getUrl();
        $this->scopeConfig = $scopeConfig;
        $this->pageFactory = $pageFactory;
        $this->message = $context->getMessageManager();
        parent::__construct($context);
    }

    public function execute()
    {
        $storeScope = ScopeInterface::SCOPE_STORE;
        $enable = $this->scopeConfig->getValue(self::XML_PATH_MODULE_ON, $storeScope);
        if($enable) {
            $page = $this->pageFactory->create();
            if ($this->scopeConfig->getValue(self::XML_PATH_META_TITLE, $storeScope)) {
                $page->getConfig()->getTitle()->set($this->scopeConfig->getValue(self::XML_PATH_META_TITLE, $storeScope));
            }
            if ($this->scopeConfig->getValue(self::XML_PATH_META_DESC, $storeScope)) {
                $page->getConfig()->setDescription($this->scopeConfig->getValue(self::XML_PATH_META_DESC, $storeScope));
            }
            if ($this->scopeConfig->getValue(self::XML_PATH_META_KEYWORDS, $storeScope)) {
                $page->getConfig()->setKeywords($this->scopeConfig->getValue(self::XML_PATH_META_KEYWORDS, $storeScope));
            }
            return $page;
        }else{
            $this->_redirect('');
        }
    }

}
