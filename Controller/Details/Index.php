<?php

namespace Brainfed\ProductExpiration\Controller\Details;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Index extends Action
{
    const XML_PATH_MODULE_ON = 'brainfed_expiration/general/enable';

    protected $session;
    protected $url;
    protected $message;
    protected $scopeConfig;
    protected $pageFactory;

    protected $productRepositoryInterface;

    public function __construct(
        Context $context,
        Session $session,
        ScopeConfigInterface $scopeConfig,
        PageFactory $pageFactory,
        ProductRepositoryInterface $productRepositoryInterface
    )
    {
        $this->session = $session;
        $this->url = $context->getUrl();
        $this->scopeConfig = $scopeConfig;
        $this->pageFactory = $pageFactory;
        $this->message = $context->getMessageManager();
        $this->productRepositoryInterface = $productRepositoryInterface;
        parent::__construct($context);
    }

    public function execute()
    {
        $storeScope = ScopeInterface::SCOPE_STORE;
        $enable = $this->scopeConfig->getValue(self::XML_PATH_MODULE_ON, $storeScope);
        if($enable) {
            if (!$productId = $this->getRequest()->getParam('product')) {
                $this->_redirect('product-expiration');
            }
            try {
                $product = $this->productRepositoryInterface->getById($productId);
                $page = $this->pageFactory->create();
                $page->getConfig()->getTitle()->set(__('Expiration Information for '.$product->getName()));
                $block = $page->getLayout()->getBlock('expiration.details');
                $block->setData('product', $product);
                return $page;
            } catch (\Exception $e) {
                $this->_redirect('product-expiration');
            }
        }else{
            $this->_redirect('');
        }
    }

}
