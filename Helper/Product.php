<?php

namespace Brainfed\ProductExpiration\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Helper\Image;
use Magento\Framework\App\Helper\Context;

class Product extends AbstractHelper
{

    protected $imageHelper;

    public function __construct(
        Context $context,
        Image $imageHelper
    ) {
        parent::__construct($context);
        $this->imageHelper = $imageHelper;
    }

    public function getImageUrl($product, $type = 'product_thumbnail_image')
    {
        return $this->imageHelper->init($product, $type)->getUrl();
    }

}
