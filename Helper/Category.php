<?php

namespace Brainfed\ProductExpiration\Helper;

use \Magento\Catalog\Model\ResourceModel\Category\TreeFactory;

class Category
{

    protected $categoryTreeFactory;

    public function __construct(
        TreeFactory $categoryTreeFactory
    )
    {
        $this->categoryTreeFactory = $categoryTreeFactory;
    }

    /**
     * Retrieve categories
     *
     * @param integer $parent
     * @param integer $recursionLevel
     * @param boolean|string $sorted
     * @param boolean $asCollection
     * @param boolean $toLoad
     * @return \Magento\Framework\Data\Tree\Node\Collection|\Magento\Catalog\Model\ResourceModel\Category\Collection
     */
    public function getCategories($parent, $recursionLevel = 0, $sorted = false, $asCollection = false, $toLoad = true)
    {
        $tree = $this->categoryTreeFactory->create();
        /* @var $tree \Magento\Catalog\Model\ResourceModel\Category\Tree */
        $nodes = $tree->loadNode($parent)->loadChildren($recursionLevel)->getChildren();

        $tree->addCollectionData(null, $sorted, $parent, $toLoad, false);

        if ($asCollection) {
            return $tree->getCollection();
        }
        return $nodes;
    }

}
