<?php

namespace Brainfed\ProductExpiration\Model\Config\Source;

use Magento\Catalog\Model\Product\Attribute\Repository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Api\FilterBuilder;

class AttributeList implements OptionSourceInterface
{

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Repository
     */
    private $attributeRepository;

    public function __construct(
        Repository $attributeRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder
    ){
        $this->attributeRepository = $attributeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
    }

    public function toOptionArray()
    {
        /**
         * Set search
         */
        $filter = $this->filterBuilder
            ->setField('frontend_input')
            ->setConditionType('eq')
            ->setValue('date')
            ->create();
        $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter])->create();

        /**
         * Get the attributes
         */
        $attributes = $this->attributeRepository->getList($searchCriteria);

        /**
         * Format the attributes and return them
         */
        $result = [];
        foreach ($attributes->getItems() as $attribute) {
            $result[] = [
                'value' => $attribute->getAttributeCode(),
                'label' => $attribute->getDefaultFrontendLabel()
            ];
        }
        return $result;
    }

}
