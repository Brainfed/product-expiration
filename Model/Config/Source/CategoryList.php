<?php

namespace Brainfed\ProductExpiration\Model\Config\Source;

use Magento\Catalog\Model\Category as ModelCategory;
use \Magento\Framework\Data\OptionSourceInterface;
use \Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Store\Model\StoreManagerInterface;
use \Magento\Framework\App\Request\Http;
use \Magento\Catalog\Model\CategoryFactory;
use \Magento\Framework\Data\CollectionFactory;
use Brainfed\ProductExpiration\Helper\Category as CategoryHelper;

class CategoryList implements OptionSourceInterface
{

    protected $storeManager;
    protected $categoryRepositoryInterface;
    protected $request;
    protected $categoryFactory;
    protected $dataCollectionFactory;
    protected $categoryHelper;

    protected $storeRootCategory;

    public function __construct(
        StoreManagerInterface $storeManager,
        CategoryRepositoryInterface $categoryRepositoryInterface,
        Http $request,
        CategoryFactory $categoryFactory,
        CollectionFactory $dataCollectionFactory,
        CategoryHelper $categoryHelper
    ) {
        $this->storeManager = $storeManager;
        $this->categoryRepositoryInterface = $categoryRepositoryInterface;
        $this->request = $request;
        $this->categoryFactory = $categoryFactory;
        $this->dataCollectionFactory = $dataCollectionFactory;
        $this->categoryHelper = $categoryHelper;
        $this->storeManager->setCurrentStore($this->request->getParam('store'));
    }

    /**
     * Generate the options for the category selector dropdown
     * @return array
     * @throws NoSuchEntityException
     */
    public function toOptionArray()
    {
        /**
         * Get the root category
         */
        $this->storeRootCategory = $this->categoryRepositoryInterface->get($this->storeManager->getStore()->getRootCategoryId());
        /**
         * Add the root category to the array
         */
        $result[] = ['label' => $this->storeRootCategory->getName(), 'value' => $this->storeRootCategory->getId()];
        /**
         * Get the rest of the categories
         */
        $categoryArray = $this->getCategoryArray();
        /**
         * Format the categories and return them
         */
        foreach ($categoryArray as $key => $value) {
            $result[] = [
                'value' => $key,
                'label' => $value
            ];
        }
        return $result;
    }

    /**
     * Get the categories for the current store and begin processing them
     * @return array
     * @throws NoSuchEntityException
     */
    private function getCategoryArray()
    {
        $category = $this->categoryFactory->create();
        /* @var $category ModelCategory */
        if (!$category->checkId($this->storeManager->getStore()->getRootCategoryId())) {
            $categories = $this->dataCollectionFactory->create();
        } else {
            $categories = $this->categoryHelper->getCategories($this->storeManager->getStore()->getRootCategoryId(), 0, false, false, true);
        }
        return $this->formatCategories($categories, $this->storeRootCategory->getName() . ' > ');
    }

    /**
     * Add the categories to an array with their id as the key and display formatted path as the value
     * @param $categories
     * @param $parentPath
     * @return array
     */
    private function formatCategories($categories, $parentPath)
    {
        $formattedCategories = array();
        foreach ($categories as $category) {
            /* @var $category ModelCategory */
            $formattedCategories[$category->getEntityId()] = $parentPath . $category->getName();
            if ($category->hasChildren()) {
                /**
                 * Recursively process children
                 */
                $formattedCategories = array_replace($formattedCategories, $this->formatCategories($category->getChildren(), $parentPath . $category->getName() . ' > '));
            }
        }
        return $formattedCategories;
    }

}
