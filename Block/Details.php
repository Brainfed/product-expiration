<?php

namespace Brainfed\ProductExpiration\Block;

use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\ScopeInterface;
use Brainfed\ProductExpiration\Helper\Product;


class Details extends \Magento\Framework\View\Element\Template
{

    const XML_PATH_CONTACT_URL = 'brainfed_expiration/general/contact_url';
    const XML_PATH_EXPIRATION_ATTRIBUTE = 'brainfed_expiration/general/expiration_attribute';

    protected $helper;
    protected $productFactory;
    protected $timezone;
    protected $scopeConfig;
    protected $expirationAttribute;

    public function __construct(
        Template\Context $context,
        CollectionFactory $productFactory,
        TimezoneInterface $timezone,
        ScopeConfigInterface $scopeConfig,
        Product $helper,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->productFactory = $productFactory;
        $this->helper = $helper;
        $this->timezone = $timezone;
        $this->scopeConfig = $scopeConfig;
        $this->expirationAttribute = $this->scopeConfig->getValue(self::XML_PATH_EXPIRATION_ATTRIBUTE, ScopeInterface::SCOPE_STORE);
    }

    public function getImage($product)
    {
        return $this->helper->getImageUrl($product, 'product_page_main_image');
    }

    public function getContactUrl()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_CONTACT_URL, ScopeInterface::SCOPE_STORE);
    }

    public function getExpirationDate($product)
    {
        return $this->timezone->formatDate($product->getData($this->expirationAttribute));
    }

}
