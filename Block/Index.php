<?php

namespace Brainfed\ProductExpiration\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Category\Tree;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Brainfed\ProductExpiration\Helper\Product;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Cms\Api\PageRepositoryInterface;

class Index extends \Magento\Framework\View\Element\Template
{

    const XML_PATH_EXPIRATION_CATEGORY = 'brainfed_expiration/general/category';
    const XML_PATH_EXPIRATION_ATTRIBUTE = 'brainfed_expiration/general/expiration_attribute';
    const XML_PATH_CMS_PAGE = 'brainfed_expiration/content/cms_page';

    protected $helper;
    protected $productFactory;
    protected $categoryRepository;
    protected $tree;
    protected $storeManager;
    protected $scopeConfig;
    protected $productRepository;
    protected $pageRepository;

    protected $expirationAttribute;

    public function __construct(
        Template\Context $context,
        CollectionFactory $productFactory,
        CategoryRepositoryInterface $categoryRepository,
        Tree $tree,
        StoreManagerInterface $storeManager,
        Product $helper,
        ScopeConfigInterface $scopeConfig,
        ProductRepositoryInterface $productRepository,
        PageRepositoryInterface $pageRepository,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->productFactory = $productFactory;
        $this->categoryRepository = $categoryRepository;
        $this->tree = $tree;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
        $this->productRepository = $productRepository;
        $this->pageRepository = $pageRepository;
        $this->expirationAttribute = $this->scopeConfig->getValue(self::XML_PATH_EXPIRATION_ATTRIBUTE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param Collection $productCollection
     * @return mixed
     */
    protected function filterBySearch($productCollection)
    {
        if($search = $this->getRequest()->getParam('search')) {
            $productCollection->addAttributeToFilter(
                array(
                    array('attribute' => 'name','like' => "%$search%"),
                    array('attribute' => 'sku','like' => "%$search%")
                )
            );
        }
        return $productCollection;
    }

    public function getAllProducts()
    {
        $productCollection = $this->productFactory->create();
        $productCollection->addAttributeToSelect('name');
        $productCollection->addAttributeToSelect('image');
        $productCollection->addAttributeToSelect($this->expirationAttribute);
        return $productCollection;
    }

    public function getImage($product)
    {
        return $this->helper->getImageUrl($this->productRepository->getById($product->getId()), 'product_page_main_image');
    }

    public function getCmsPage()
    {
        try {
            $cmsPageId = $this->scopeConfig->getValue(self::XML_PATH_CMS_PAGE, ScopeInterface::SCOPE_STORE);
            return $this->pageRepository->getById($cmsPageId);
        } catch (\Exception $e) {
            /**
             * TODO Handle CMS page not set
             */
        }


    }

}
